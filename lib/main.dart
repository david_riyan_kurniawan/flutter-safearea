import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: HomePage(),
    );
  }
}

class HomePage extends StatelessWidget {
  HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: const Text('Safe Area'),
      // ),
      body: SafeArea(
        //dengan adanya widget ini kita dapat mengamankan aplikasi kita dari frame yang ada di perangkat user
        // maintainBottomViewPadding: false,
        // minimum: EdgeInsets.symmetric(horizontal: 20), => berguna untuk menentukan jarak yang diterapkan untuk sebuah frame layar
        child: ListView(
          children: [
            Column(
              children: const [
                Center(
                  child: Text(
                    'HELLO WORLD',
                    style: TextStyle(fontSize: 50),
                  ),
                ),
                Center(
                  child: Text(
                    'HELLO WORLD',
                    style: TextStyle(fontSize: 50),
                  ),
                ),
                Center(
                  child: Text(
                    'HELLO WORLD',
                    style: TextStyle(fontSize: 50),
                  ),
                ),
                Center(
                  child: Text(
                    'HELLO WORLD',
                    style: TextStyle(fontSize: 50),
                  ),
                ),
                Center(
                  child: Text(
                    'HELLO WORLD',
                    style: TextStyle(fontSize: 50),
                  ),
                ),
                Center(
                  child: Text(
                    'HELLO WORLD',
                    style: TextStyle(fontSize: 50),
                  ),
                ),
                Center(
                  child: Text(
                    'HELLO WORLD',
                    style: TextStyle(fontSize: 50),
                  ),
                ),
                Center(
                  child: Text(
                    'HELLO WORLD',
                    style: TextStyle(fontSize: 50),
                  ),
                ),
                Center(
                  child: Text(
                    'HELLO WORLD',
                    style: TextStyle(fontSize: 50),
                  ),
                ),
                Center(
                  child: Text(
                    'HELLO WORLD',
                    style: TextStyle(fontSize: 50),
                  ),
                ),
                Center(
                  child: Text(
                    'HELLO WORLD',
                    style: TextStyle(fontSize: 50),
                  ),
                ),
                Center(
                  child: Text(
                    'HELLO WORLD',
                    style: TextStyle(fontSize: 50),
                  ),
                ),
                Center(
                  child: Text(
                    'HELLO WORLD',
                    style: TextStyle(fontSize: 50),
                  ),
                ),
                Center(
                  child: Text(
                    'HELLO WORLD',
                    style: TextStyle(fontSize: 50),
                  ),
                ),
                Center(
                  child: Text(
                    'HELLO WORLD',
                    style: TextStyle(fontSize: 50),
                  ),
                ),
                Center(
                  child: Text(
                    'HELLO WORLD',
                    style: TextStyle(fontSize: 50),
                  ),
                ),
                Center(
                  child: Text(
                    'HELLO WORLD',
                    style: TextStyle(fontSize: 50),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
